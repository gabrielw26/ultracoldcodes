## Planned meeting
* **Meeting 2** _(around Jan/Feb 2022)_:  
  See [here](https://gitlab.com/coldatoms/ultracoldcodes/-/issues/1) for requested topics to be discussed. 

## List of past meetings
* **Meeting 1** _(03-11-2021)_:  
  Initial meeting. Discussion of the main idea of this repo. 
