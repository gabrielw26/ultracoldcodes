[[_TOC_]]

## Mean-field like codes
| Code | Description |  Link |
| ---  | ---         |  ---  |
| W-SLDA Toolkit | The toolkit allows for simulating ultracold atomic gases of **fermionic** type by means of **Density Functional Theory**. Both static and time-depend phenomena can be investigated by means of W-SLDA. The software is optimized for simulations of large systems, consisting of thousands of particles. Particularly it is well suited to study static and dynamic properties of ultracold Fermi gases across BCS - BEC crossover. W-SLDA Toolkit implements functionals for reliable studies of systems being in weakly interacting BCS regime (BdG functional) and in the strongly interacting regime, called unitary Fermi gas (SLDA functional).| [webpage](https://wslda.fizyka.pw.edu.pl/)<br> [git repo](https://gitlab.fizyka.pw.edu.pl/wtools/wslda)|
| MUDGE | The program enables to simulate **dipolar Bose** gases in a **quasi-1D trap**. It implements a few approaches towards the Bose gases including i.a. the Gross-Pitaevskii equation and its modified version for intermediate and strong interaction called the Lieb-Liniger GPE. This tool provides you with the posiibility of evolution in imaginary and real time. |[git repo](https://gitlab.com/jakkop/mudge/-/tags/v01Jun2021)|
| Super_Hydro |**Superfluid hydrodynamics explorer**. This project provides a client-server interface for exploring superfluid dynamics. Communication can take place over the network (using ZMQ) allowing a local client to connect with a computation server running on a high-performance computer with GPU acceleration. (The server can also be run locally.) |[git repo](https://github.com/mforbes/super_hydro)|

## Data formats
| Code | Description |  Link |
| ---  | ---         |  ---  |
| W-Data | W-data format is designed to store and manipulate data defined on a spatial lattice. This format was originally derived from the [W-SLDA](https://gitlab.fizyka.pw.edu.pl/wtools/wslda) project. The format is designed to be conceptually easy to understand, which turns out to be an important issue for academic applications where students are typically involved. Presently the format support lattices in 1D, 2D, and 3D. Real, complex, and vector variables can be stored. | [git repo](https://gitlab.fizyka.pw.edu.pl/wtools/wdata) |

## Data analysis tools
| Code | Description |  Link |
| ---  | ---         |  ---  |
| W-Deriv | Simple C library for computation derivatives of functions defined on a lattice. The library uses spectral methods to achieve high accuracy. | [git repo](https://gitlab.fizyka.pw.edu.pl/wtools/wderiv) |
| W-Interp | C99 library for interpolating data defined on a spatial lattice. The library uses spectral methods for high-quality results | [git repo](https://gitlab.fizyka.pw.edu.pl/wtools/winterp) | 
