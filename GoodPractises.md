[[_TOC_]]

## Documentation
It is obvious that documentation is the first thing that potential user is looking for. We recommend:
* Provide at least simple documentation in Markdown format. Preferably, it should be accessible in README.md file, that is automatically displayed by each git-type repo. 
* Minimal documentation should contain:
  * Description of physical problem that the code solves.
  * How to compile and run the code.
  * Example usage of the code.

## Data formats
In order to be able to create workflows, unification of data format is desirable. For now, by workflow, we mean that the output of one code/tool becomes the input for another. From a variety of available formats here we recommend using primarily (if possible): 
1. **Plain text format**:  it is the simplest format that can be processed by a variety of tools (for example by `numpy.loadtxt(...)`). We strongly recommend providing in the header of the file description of columns meaning. For example:  
```
# DESCRIPTION OF CONTENT
# ...
# COLUMNS:
# 1: vortex id
# 2: x-coordinate of the vortex core
# 3: y-coordinate of the vortex core
     0    35.4702   40.0164
     1    15.3489   39.7518
...
```
2. **[W-Data](https://gitlab.fizyka.pw.edu.pl/wtools/wdata)**: W-data format is designed to store and manipulate binary data defined on a spatial lattice. The format is designed to be conceptually easy to understand, which turns out to be an important issue for academic applications where students are typically involved. Presently there are available [C](https://gitlab.fizyka.pw.edu.pl/wtools/wdata) and [python](https://hg.iscimath.org/forbes-group/wdata) libs supporting the format. The format is also integrated with [VisIt](https://visit-dav.github.io/visit-website/index.html) tool. 

## Citing/Acknowledgment standards
It is recommended to provide info in the documentation on how to cite or acknowledge your code in case of usage. 
